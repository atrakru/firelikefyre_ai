import os
import json
import boto3
import requests
import tempfile

s3_client = boto3.client('s3')

def lambda_handler(event, context):
    bucket = event['detail']['bucket']['name']
    key = event['detail']['object']['key']
    download_path = os.path.join(tempfile.gettempdir(), key)

    s3_client.download_file(bucket, key, download_path)
    status_code = upload_to_pinata(download_path)
    
    print(f'Pinata API returned status code: {status_code}')
    return {
        'Status Code': status_code
    }

def upload_to_pinata(file_path):
    url = "https://api.pinata.cloud/pinning/pinFileToIPFS"
    headers = {
        "Authorization": "Bearer " + os.environ['PINATA_JWT'],
    }

    with open(file_path, 'rb') as fp:
        image_binary = fp.read()

    # storing the filename as a variable
    unique_filename = os.path.basename(file_path)

    # Using the file name in the pinataMetadata
    # metadata additions TBD
    payload = {
        'pinataOptions': '{"cidVersion": 1}',
        'pinataMetadata': '{"name": "' + unique_filename + '", "keyvalues": {"company": "Pinata"}}'
    }

    response = requests.post(
        url,
        headers=headers,
        data=payload,
        files={"file": (unique_filename, image_binary, 'application/octet-stream')}
    )

    return response.status_code

